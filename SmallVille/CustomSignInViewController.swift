//
//  CustomSignInViewController.swift
//  SmallVille
//
//  Created by Anuj Patel on 2/15/19.
//  Copyright © 2019 Anuj Patel. All rights reserved.
//

import UIKit
import Firebase
import Alamofire


var customServerToken:String = ""
class CustomSignInViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var authenticationDescriptionLabel: UILabel!

    // Fetching the customizable Server endpoint from the info.plist
    let targetSite:String = Bundle.main.object(forInfoDictionaryKey: "Target Site")! as! String
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextFields()
    }
    
    private func configureTextFields(){
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    
    @IBAction func submitCustomSignIn(_ sender: Any) {
        var isEmailValid:Bool
        var isPasswordValid:Bool
        
            // Form validation for email and password
            if (emailTextField.text != nil && emailTextField.text != ""){
                isEmailValid = true
                print("Email: ", emailTextField.text!)
            }else {
                print("Email is mandatory. Make sure the email field is not empty.")
                isEmailValid = false
            }
        
            if (passwordTextField.text != nil && passwordTextField.text != ""){
                isPasswordValid = true
                print("Password: ", passwordTextField.text!)
            }else{
                isPasswordValid = false
                print("Password is mandatory. Make sure the password field is not empty.")
            }
        
            // Check if email and passwords are valid (client side) before submitting the form.
            if (isEmailValid && isPasswordValid){
                print("Email and Password validation completed successfully.")
                
            // Check if the user and password are correct by calling the /login API.
                
                // Gathering the API to be called
               
                
                if (Bundle.main.object(forInfoDictionaryKey: "Target Site") != nil){
                    let targetSite = Bundle.main.object(forInfoDictionaryKey: "Target Site")
                    print(targetSite!)
                }
               
                 let urlString = "\(targetSite)/login"
                
                // Gathering request parameters
                let requestParameters = [
                    "email": "\(emailTextField.text ?? "")",
                    "password": "\(passwordTextField.text ?? "")"
                ]

                
                // Fetch the customServerToken from the server response.
                // Calling the /login  API. Bring up a spinning wheel on the UI.
                AF.request(urlString, method: .post, parameters: requestParameters,encoding: JSONEncoding.default, headers: nil).responseJSON {
                    response in
                    switch response.result {
                    case .success:
                        print("***** Response JSON *****")
                        print(response)
                        
                        
                        //to get status code
                        if let status = response.response?.statusCode {
                            switch(status){
                            case 200:
                                print("\(status) - Authentication Successful!")
                                self.authenticationDescriptionLabel.textColor = UIColor(red: 92/255, green: 197/255, blue: 167/255, alpha: 1.0)

                                self.authenticationDescriptionLabel.text = "Authentication Successful!"
                                
                                //to get JSON return value
                                if let result = response.result.value {
                                    let JSON = result as! NSDictionary
                                    
                                    if (JSON["token"] != nil) {
                                       customServerToken = JSON["token"] as! String
                                    }
                                 
                                    // Use the customServerToken to generate the IDToken which will be sent to the server in subsequent API calls for validating session and user identity.
                                    let VC = ViewController()
                                    Auth.auth().signIn(withCustomToken: customServerToken ) { (currentUser, error) in
                                        // ...
                                        if let error = error{
                                            print("Failed to generate Firebase ID Token on the client with error: ", error )
                                            return
                                        }
                                        VC.getFirebaseIDToken()
                                        
                                    }
                                    
                                }
                            
                            // Authentication failed. Sending an alert message to the user.
                            case 401:
                                print("\(status) - Authentication Failed. Incorrect credentials enetered.")
                                self.authenticationDescriptionLabel.textColor = UIColor.red
                                self.authenticationDescriptionLabel.text = "Authentication Failed. Incorrect credentials enetered."
                            case 400:
                                print("\(status) - Authentication Failed. This user does not exist.")
                                self.authenticationDescriptionLabel.textColor = UIColor.red
                                self.authenticationDescriptionLabel.text = "Authentication Failed. This user does not exist."
                            default:
                                print("\(status) - Unknown error with response status: \(status)")
                                self.authenticationDescriptionLabel.textColor = UIColor.red
                                self.authenticationDescriptionLabel.text = "Authentication Failed. Unknown error."
                            }
                        }
                        
                        
                        break
                    case .failure(let error):
                        print(error)
                    }
                }

                
                
                
                
                
                
      
            
        }
    }
    
    
    @IBAction func cancelCustomSignIn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    

    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// Handling 'Next' and 'return' for dismissing the keyboard for text fields
extension CustomSignInViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == passwordTextField {
            self.view.endEditing(true)
        } else {
            passwordTextField.becomeFirstResponder()
        }
        return true
    }
}
