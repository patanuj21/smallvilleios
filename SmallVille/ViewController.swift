//
//  ViewController.swift
//  SmallVille
//
//  Created by Anuj Patel on 11/11/18.
//  Copyright © 2018 Anuj Patel. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import Alamofire

class ViewController: UIViewController, GIDSignInUIDelegate{
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var customFBButton: UIButton!
    @IBOutlet weak var customGoogleButton: UIButton!
    


    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        GIDSignIn.sharedInstance()?.uiDelegate = self
        
        
        // Adding custom Google Login Button
        customGoogleButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        customGoogleButton.addTarget(self, action:  #selector(handleCustomGoogleLogin), for: .touchUpInside)
        
        
        // Adding custom FB Login Button
        customFBButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        customFBButton.addTarget(self, action: #selector(handleCustomFBLogin), for: .touchUpInside)
    }
    
    // Call the Google SDK using the custom button
    @objc func handleCustomGoogleLogin(){
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    // Call the FB SDK using the custom button
    @objc func handleCustomFBLogin() {
        FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, err) in
            if err != nil {
                print("Custom FB login failed :" , err ?? "")
                return
            }
                print("Custom FB login successful")
                self.showEmailAddress()
        }
    }
 

    // Query FB for email as well as generate a Firebase User
    func showEmailAddress(){
        
        let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                // ...
                print("Error when creating Firebase Facebook user: \(error)")
                return
            }
            
            print("***** Logged in to Firebase with below Facebook credentials *****")
            print("Email: ", authResult?.user.email ?? "" )
            print("Display Name: ", authResult?.user.displayName ?? "" )
            print("UID: ", authResult?.user.uid ?? "")
            // User is signed in
                // ...
        }
        
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email"])?.start(completionHandler: { (connection, result, err) in
            if err != nil{
                print("Failed to complete graph request: ", err ?? "")
                return
            }
            
            print("Printing Facebook Graph result: ", result ?? "")
            
        })
        
        getFirebaseIDToken()
        
    }

    // Sign in with Email-Password
    @IBAction func customSignIn(_ sender: Any) {

      
    }

    
    func getFirebaseIDToken(){
        
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                print("Error while generating Firebase ID Token after sucessfully signing in: ", error ?? "")
                return
            }
            print("The ID Token to be sent to the beckend for validation is: ", idToken ?? "")
            print("***** Printing Firebase current user details ****")
            print(currentUser?.email ?? "")
            // Send token to your backend via HTTPS
            // ...
        }
        
    }
    
    

}

